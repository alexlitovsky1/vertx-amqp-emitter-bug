package com.alexlitovsky.bugs.amqp;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.scheduler.Scheduled;

@ApplicationScoped
public class MessageSender {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageSender.class);
	
	@Inject
    @Channel("test")
    Emitter<String> emitter;

	@Scheduled(every = "{sender.period}", delayed = "5s")
	public void sendMessage() {
		try {
			emitter.send("Hello world").toCompletableFuture().get(1, TimeUnit.SECONDS);
			LOGGER.info("sent message");
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			throw new RuntimeException(e);
		}
	}
}
